package game;

public class Effect implements EffectInterface {
	protected int duration;
	
	@Override
	public boolean isDone() { return duration < 1; }

	public Effect(int duration){
		this.duration = duration;
	}
	
	public Effect(Effect other){
		this.duration = other.duration; 
	}
	
	@Override
	public void update(Creature creature){
		duration--;
	}
	
	@Override
	public void start(Creature creature){
		
	}
	
	@Override
	public void end(Creature creature){
		
	}
}
