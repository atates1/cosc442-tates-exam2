package game;

public interface EffectInterface {

	boolean isDone();

	void update(Creature creature);

	void start(Creature creature);

	void end(Creature creature);

}